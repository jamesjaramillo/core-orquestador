package pe.com.hiper.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import pe.com.hiper.common.UtilWeb;

@Entity
@Table(name = "tpcomponentelog")
public class TpComponenteLog {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ccomponentid")
	private Long cComponentId;
	
	@Column(name = "cdsterminal", nullable = false)
	private String cDsTerminal;
	
	@Column(name = "cdstrace", nullable = false)
	private String cDsTrace;
	
	@Column(name = "ttxmethod", nullable = false)
	private String tTxMethod;
	
	@Column(name = "htxserviceinput", nullable = false)
	private String hTxServiceInput;
	
	@Column(name = "htxserviceoutput", nullable = false)
	private String hTxServiceOutput;
	
	@Column(name = "ctxtypeservice", nullable = false)
	private String cTxTypeService;
	
	@Column(name = "ntxerror", nullable = false)
	private int nTxError;
	
	@Column(name = "dtxservicedate", nullable = false)
	private String dTxServiceDate;
	
	
	public TpComponenteLog(String cDsTerminal, String cDsTrace, String tTxMethod) {
		super();
		this.cDsTerminal = cDsTerminal;
		this.cDsTrace = cDsTrace;
		this.tTxMethod = tTxMethod;
		this.nTxError = 1;
		this.dTxServiceDate = UtilWeb.getFechaActual();
	}
	
	
	public TpComponenteLog() {
		super();
	}

	public Long getcComponentId() {
		return cComponentId;
	}


	public void setcComponentId(Long cComponentId) {
		this.cComponentId = cComponentId;
	}


	public String getcDsTerminal() {
		return cDsTerminal;
	}
	public void setcDsTerminal(String cDsTerminal) {
		this.cDsTerminal = cDsTerminal;
	}
	public String getcDsTrace() {
		return cDsTrace;
	}
	public void setcDsTrace(String cDsTrace) {
		this.cDsTrace = cDsTrace;
	}
	public String gettTxMethod() {
		return tTxMethod;
	}
	public void settTxMethod(String tTxMethod) {
		this.tTxMethod = tTxMethod;
	}
	public String gethTxServiceInput() {
		return hTxServiceInput;
	}
	public void sethTxServiceInput(String hTxServiceInput) {
		this.hTxServiceInput = hTxServiceInput;
	}
	public String gethTxServiceOutput() {
		return hTxServiceOutput;
	}
	public void sethTxServiceOutput(String hTxServiceOutput) {
		this.hTxServiceOutput = hTxServiceOutput;
	}
	public String getcTxTypeService() {
		return cTxTypeService;
	}
	public void setcTxTypeService(String cTxTypeService) {
		this.cTxTypeService = cTxTypeService;
	}

	public int getnTxError() {
		return nTxError;
	}

	public void setnTxError(int nTxError) {
		this.nTxError = nTxError;
	}


	public String getdTxServiceDate() {
		return dTxServiceDate;
	}


	public void setdTxServiceDate(String dTxServiceDate) {
		this.dTxServiceDate = dTxServiceDate;
	}
	
	
	
	

}

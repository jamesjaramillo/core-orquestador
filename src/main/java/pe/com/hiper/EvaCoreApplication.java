package pe.com.hiper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EvaCoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(EvaCoreApplication.class, args);
	}

}

package pe.com.hiper.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import pe.com.hiper.model.Moneda;

public class UtilWeb {
	
	public final static String SERVICE_CORE="COREMS3K";
	public final static String SERVICE_HC="WHCENTERCORE";
	public final static String SERVICE_LUCES="LUCES";
	public final static String SERVICE_DIVERTOR="DIVERTOR";
	public final static String SERVICE_MONEDERO="MONEDERO";
	public final static String SERVICE_BILLETERO="BILLETERO";
	public final static String SERVICE_IMPRESORA="IMPRESORA";
	public final static String SERVICE_SMARTHOPPER="SMARTHOPPER";
	public final static String SERVICE_HOPPER="HOPPER";
	public final static String SERVICE_CASTLE="CASTLE";
	public final static String SERVICE_SENSOR="SENSOR";
	
	public static boolean KIOSCO_EN_USO=false;
	public static boolean KIOSCO_EN_RED=true;
	
	
	
	public static String KEY1="1A3F5B68A93C34E5";
	public static String KEY2="B61A3F53C8A934E5";
	public static String KEY3="153C3A3F4E5B68A9";
	
	public static HttpHeaders getHeader() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		return headers;
	}
	
	public static String getHoraActual() {
		Locale currentLocale = new Locale("es", "PE");
		Date today = new Date();
		DateFormat formatter = new SimpleDateFormat("HH:mm:ss:SSS", currentLocale);
		String result = formatter.format(today);
		return result;
	}
	
	public static String getFechaActual() {
		Locale currentLocale = new Locale("es", "PE");
		Date today = new Date();
		DateFormat formatter = new SimpleDateFormat("ddMMyyyy", currentLocale);
		String result = formatter.format(today);
		return result;
	}

}

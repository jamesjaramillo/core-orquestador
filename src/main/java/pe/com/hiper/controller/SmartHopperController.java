package pe.com.hiper.controller;

import java.math.BigDecimal;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.hiper.api.service.SmartHopperService;

@RestController
@RequestMapping(path = "/evaCore/api/v1/smartHopper")
public class SmartHopperController {
	
	@Autowired
	SmartHopperService smartHopperService;
	
	@PostMapping(path = "/estadoSMH", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> estadoSensor(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = smartHopperService.estadoSMH(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/dispensarMonto", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> dispensarMonto(@RequestBody HashMap<String, Object> input) throws Exception {

		BigDecimal bMonto=new BigDecimal(input.get("monto").toString());
		HashMap<String, String> data = smartHopperService.dispensarMonto(input.get("trace").toString(), bMonto.intValue());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/agregarMonedas", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> agregarMonedas(@RequestBody HashMap<String, Object> input) throws Exception {

		Double denominacion=Double.parseDouble(input.get("denominacion").toString());
		HashMap<String, String> data = smartHopperService.agregarMonedas(input.get("trace").toString(), Integer.parseInt(input.get("cantidad").toString()), denominacion);

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/vaciarSmartHopper", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> vaciarSmartHopper(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = smartHopperService.vaciarSmartHopper(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/obtenerMonedas", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> obtenerMonedas(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = smartHopperService.obtenerMonedas(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

}

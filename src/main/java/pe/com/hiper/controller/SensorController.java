package pe.com.hiper.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.hiper.api.service.SensorService;

@RestController
@RequestMapping(path = "/evaCore/api/v1/sensor")
public class SensorController {
	
	@Autowired
	SensorService sensorService;
	
	@PostMapping(path = "/estadoSensor", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> estadoSensor(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = sensorService.estadoSensor(input.get("trace").toString(), input.get("numeroPin").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

}

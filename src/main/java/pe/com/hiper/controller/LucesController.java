package pe.com.hiper.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.hiper.api.service.LucesService;

@RestController
@RequestMapping(path = "/evaCore/api/v1/luces")
public class LucesController {

	@Autowired
	LucesService lucesService;
	
	@PostMapping(path = "/encenderLuces", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> encenderLuces(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = lucesService.encenderLuces(input.get("trace").toString(), input.get("dispositivo").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/apagarLuces", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> apagarLuces(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = lucesService.apagarLuces(input.get("trace").toString(), input.get("dispositivo").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
}

package pe.com.hiper.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.hiper.api.service.ImpresoraService;

@RestController
@RequestMapping(path = "/evaCore/api/v1/impresora")
public class ImpresoraController {
	
	@Autowired
	ImpresoraService impresoraService;
	
	@PostMapping(path = "/estadoImpresora", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> estadoImpresora(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = impresoraService.estadoImpresora(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/realizarImpresion", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> realizarImpresion(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = impresoraService.realizarImpresion(input.get("texto").toString(), "imagen", input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

}

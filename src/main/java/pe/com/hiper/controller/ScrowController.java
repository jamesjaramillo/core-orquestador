package pe.com.hiper.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.hiper.api.service.ScrowService;

@RestController
@RequestMapping(path = "/evaCore/api/v1/scrow")
public class ScrowController {
	
	@Autowired
	ScrowService scrowService;
	
	@PostMapping(path = "/testearScrow", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> testearScrow(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = scrowService.testearScrow(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/aceptarScrow", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> aceptarScrow(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = scrowService.AceptarScrow(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/devolverScrow", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> devolverScrow(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = scrowService.DevolverScrow(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

}

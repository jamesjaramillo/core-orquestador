package pe.com.hiper.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.hiper.api.service.BilleteroService;
import pe.com.hiper.model.Componente;
import pe.com.hiper.model.Moneda;

@RestController
@RequestMapping(path = "/evaCore/api/v1/billetero")
public class BilleteroController {

	@Autowired
	BilleteroService billeteroService;

	@PostMapping(path = "/aceptarBilletes", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> aceptarBilletes(@RequestBody HashMap<String, Object> input) throws Exception {

		Moneda billetes = billeteroService.aceptarBilletes(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(billetes);
	}

	@PostMapping(path = "/cancelarScrow", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> cancelarScrow(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = billeteroService.cancelarScrow(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/finalizarScrow", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> finalizarScrow(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = billeteroService.finalizarScrow(input.get("trace").toString(),
				Boolean.parseBoolean(input.get("isCancel").toString()));

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/dispensarBilletes", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> dispensarBilletes(@RequestBody HashMap<String, Object> input) throws Exception {

		Moneda billete = billeteroService.dispensarBilletes((List<Moneda>) input.get("billetes"),
				input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(billete);
	}

	@PostMapping(path = "/vaciarPayout", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> vaciarPayout(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = billeteroService.vaciarPayout(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/resetearEquipo", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> resetearEquipo(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = billeteroService.resetEquipo(input.get("trace").toString(),
				input.get("tipoReinicio").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/solucionCashInActive", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> solucionCashInActive(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = billeteroService.solucionCashInActive(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/cancelarUltimoComando", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> cancelarUltimoComando(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = billeteroService.cancelarUltimoComando(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/estadoBilletero", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> estadoBilletero(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = billeteroService.estadoBilletero(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/habilitarBilletero", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> habilitarBilletero(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = billeteroService.habilitarBilletero(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/consultarComponent", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> consultarComponent(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, Object> data = billeteroService.consultarComponent(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/consultarComponentes", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> consultarComponentes(@RequestBody HashMap<String, Object> input) throws Exception {

		List<Componente> data = billeteroService.consultarComponentes(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/actualizarComponent", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> actualizarComponent(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = billeteroService.actualizarComponent(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/billetesPorComponente", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> billetesPorComponente(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = billeteroService.billetesPorComponente(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/configurarComponente", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> configurarComponente(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = billeteroService.configurarComponente(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/billetesIngresado", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> billetesIngresado(@RequestBody HashMap<String, Object> input) throws Exception {

		Moneda data = billeteroService.billetesIngresado(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/conectarEquipo", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> conectarEquipo(@RequestBody HashMap<String, Object> input) {

		HashMap<String, String> data = billeteroService.conectarEquipo(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

}

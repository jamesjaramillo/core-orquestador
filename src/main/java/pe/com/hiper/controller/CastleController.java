package pe.com.hiper.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.hiper.api.service.CastleService;

@RestController
@RequestMapping(path = "/evaCore/api/v1/castle")
public class CastleController {
	
	@Autowired
	CastleService castleService;
	
	@PostMapping(path = "/estadoCastle", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> cancelarScrow(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = castleService.estadoCastle(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/obtenerDatosCastle", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> obtenerDatosCastle(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = castleService.obtenerDatosCastle(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/ventaCastle", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> ventaCastle(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = castleService.ventaCastle(input.get("trace").toString(), input.get("monto").toString(), 3);

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/confirmarVentaCastle", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> confirmarVentaCastle(@RequestBody HashMap<String, Object> input) throws Exception {
		//estado 1= confirmar 2=no confirmar
		HashMap<String, String> data = castleService.confirmarVentaCastle(input.get("trace").toString(), input.get("estado").toString(), 3);

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	

}

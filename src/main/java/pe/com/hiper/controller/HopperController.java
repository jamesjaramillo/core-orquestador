package pe.com.hiper.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.hiper.api.service.HopperService;

@RestController
@RequestMapping(path = "/evaCore/api/v1/hopper")
public class HopperController {

	@Autowired
	HopperService hopperService;

	@PostMapping(path = "/estadoHopper", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> estadoHopper(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = hopperService.estadoHopper(input.get("trace").toString(),
				input.get("direccion").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/dispensarHopper", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> dispensarHopper(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = hopperService.dispensarHopper(input.get("trace").toString(),
				input.get("direccion").toString(), input.get("cantidad").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

}

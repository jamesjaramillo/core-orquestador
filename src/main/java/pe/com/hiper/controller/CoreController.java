package pe.com.hiper.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.transaction.annotation.Transactional;

import pe.com.hiper.api.bussines.EvaBussines;
import pe.com.hiper.common.UtilWeb;
import pe.com.hiper.service.TraceService;

@RestController
@RequestMapping(path = "/evaCore/api/v1/Eva")
public class CoreController {
	
	@Autowired
	TraceService traceService;
	
	@Autowired
	EvaBussines evaBussines;
	
	
	@PostMapping(path = "/getTrace", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> getTrace(@RequestBody HashMap<String, Object> input) throws Exception {
		
		HashMap<String, Object> data=new HashMap<String, Object>();
		data.put("estado", 0);
		data.put("descripcion", traceService.getTrace().getcTrace());
		
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	//@Transactional(timeout = 30)
	@PostMapping(path = "/validarDispositivos", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> validarDispositivo(@RequestBody HashMap<String, Object> input) throws Exception {
		
		
		//HashMap<String, String> data=smartHSevice.estadoSMH(input.get("trace").toString());
		List<HashMap<String, String>> data=evaBussines.validarDispositivos(input.get("accion").toString(), input.get("trace").toString());
		
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/obtenerOperatividadKiosko", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> obtenerOperatividadKiosko(@RequestBody HashMap<String, Object> input) throws Exception {
		
		HashMap<String, String> data=evaBussines.obtenerEstadoKiosco(input.get("trace").toString());
		
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/setearEstadoKiosko", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> setearOperatividadKiosko(@RequestBody HashMap<String, Object> input) throws Exception {
		
		HashMap<String, String> data=evaBussines.setearEstadoKiosco(input.get("trace").toString(), input.get("estado").toString());
		
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	
	@PostMapping(path = "/validarTicketAdmin", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> validarTicketAdmin(@RequestBody HashMap<String, Object> input) throws Exception {
		
		boolean flagRegistrar=Boolean.parseBoolean(input.get("flag").toString());
		HashMap<String, Object> data=evaBussines.validarTicketAdmin(input.get("trace").toString(), input.get("usuario").toString(),flagRegistrar);
		
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
}

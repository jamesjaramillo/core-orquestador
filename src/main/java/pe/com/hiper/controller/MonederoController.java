package pe.com.hiper.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.hiper.api.service.MonederoService;

@RestController
@RequestMapping(path = "/evaCore/api/v1/monedero")
public class MonederoController {
	
	@Autowired
	MonederoService monederoService;
	
	@PostMapping(path = "/estadoMonedero", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> estadoMonedero(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = monederoService.estadoMonedero(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/habilitarMonedero", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> habilitarMonedero(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = monederoService.habilitarMonedero(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/deshabilitarMonedero", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> deshabilitarMonedero(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = monederoService.deshabilitarMonedero(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/obtenerDenominacion", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> obtenerDenominacion(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = monederoService.obtenerDenominacion(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

}

package pe.com.hiper.api.service;

import java.util.HashMap;

public interface SensorService {
	
	public HashMap<String, String> estadoSensor(String trace, String numeroPin);

}

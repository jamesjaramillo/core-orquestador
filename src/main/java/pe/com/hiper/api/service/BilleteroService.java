package pe.com.hiper.api.service;

import java.util.HashMap;
import java.util.List;

import pe.com.hiper.model.Componente;
import pe.com.hiper.model.Moneda;

public interface BilleteroService {
	
	public Moneda aceptarBilletes(String trace);
	
	public HashMap<String, String> cancelarScrow(String trace);
	
	public HashMap<String, String> finalizarScrow(String trace, boolean isCancel);
	
	public Moneda dispensarBilletes(List<Moneda> billetes, String trace);
	
	public HashMap<String, String> vaciarPayout(String trace);
	
	public HashMap<String, String> resetEquipo(String trace, String tipoReinicio);
	
	public HashMap<String, String> solucionCashInActive(String trace);
	
	public HashMap<String, String> cancelarUltimoComando(String trace);
	
	public HashMap<String, String> estadoBilletero(String trace);
	
	public HashMap<String, String> habilitarBilletero(String trace);
	
	public HashMap<String, Object> consultarComponent(String trace);
	
	public List<Componente> consultarComponentes(String trace);	

	public HashMap<String, String> actualizarComponent(String trace);
	
	public HashMap<String, String> billetesPorComponente(String trace);
	
	public HashMap<String, String> configurarComponente(String trace);
	
	public Moneda billetesIngresado(String trace);
	
	public HashMap<String, String> conectarEquipo(String trace);

}

package pe.com.hiper.api.service;

import java.util.HashMap;

import com.google.gson.JsonArray;

public interface SmartHopperService {
	
	public HashMap<String, String> estadoSMH(String trace);
	
	public HashMap<String, String> dispensarMonto(String trace, int monto);
	
	public HashMap<String, String> dispensarMonedas(String trace, JsonArray monedas);
	
	public HashMap<String, String> agregarMonedas(String trace, int cantidad, double denominacion);
	
	public HashMap<String, String> vaciarSmartHopper(String trace);
	
	public HashMap<String, String> obtenerMonedas(String trace);

}

package pe.com.hiper.api.service;

import java.util.HashMap;

public interface LucesService {
	
	public HashMap<String, String> apagarLuces(String trace, String dispositivo);
	public HashMap<String, String> encenderLuces(String trace, String dispositivo);

}

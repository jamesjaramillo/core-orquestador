package pe.com.hiper.api.service;

import java.util.HashMap;

public interface MonederoService {
	
	public HashMap<String, String> estadoMonedero(String trace);
	
	public HashMap<String, String> habilitarMonedero(String trace);
	
	public HashMap<String, String> deshabilitarMonedero(String trace);
	
	public HashMap<String, String> obtenerDenominacion(String trace);
	

}

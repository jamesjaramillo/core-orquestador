package pe.com.hiper.api.service;

import java.util.HashMap;

public interface ScrowService {
	
	public HashMap<String, String> testearScrow(String trace);	
	public HashMap<String, String> AceptarScrow(String trace);
	public HashMap<String, String> DevolverScrow(String trace);

}

package pe.com.hiper.api.service;

import java.util.HashMap;

public interface ImpresoraService {
	
	public HashMap<String, String> estadoImpresora(String trace);
	
	public HashMap<String, String> realizarImpresion(String datoImprimir, String dataImagen, String trace);

}

package pe.com.hiper.api.service;

import java.util.HashMap;

public interface CastleService {

	public HashMap<String, String> estadoCastle(String trace);
	public HashMap<String, String> sincronizacionCastle(String trace, int nroReintentos);
	public HashMap<String, String> obtenerDatosCastle(String trace);
	public HashMap<String, String> ventaCastle(String trace, String monto, int nroReintentos);
	public HashMap<String, String> confirmarVentaCastle(String trace, String estado, int nroReintentos);
	public HashMap<String, String> confirmarCancelCastle(String trace, String estado, int nroReintentos);
}

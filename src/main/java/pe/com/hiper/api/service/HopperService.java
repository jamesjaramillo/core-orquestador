package pe.com.hiper.api.service;

import java.util.HashMap;

public interface HopperService {
	
	public HashMap<String, String> estadoHopper(String trace, String direccionHopper);
	
	public HashMap<String, String> dispensarHopper(String trace, String direccionHopper, String cantidadMonedas);

}

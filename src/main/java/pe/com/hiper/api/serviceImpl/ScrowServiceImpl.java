package pe.com.hiper.api.serviceImpl;

import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import pe.com.hiper.api.service.ScrowService;
import pe.com.hiper.common.Constantes;
import pe.com.hiper.common.UtilWeb;
import pe.com.hiper.entity.TpComponenteLog;
import pe.com.hiper.service.ComponenteLogService;

@Component
public class ScrowServiceImpl implements ScrowService {

	private static final Logger LOGGER = LogManager.getLogger(ScrowServiceImpl.class);

	@Value("${url.api.scrow}")
	private String urlApi;

	@Value("${kiosco.terminal}")
	private String nroTerminal;

	@Autowired
	private ComponenteLogService componenteService;

	@Override
	public HashMap<String, String> testearScrow(String trace) {
		LOGGER.info(trace + " INICIO testearScrow");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "testearScrow");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_DIVERTOR);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject input = new JSONObject();
			input.put("trace", trace);
			LOGGER.info(trace + " INPUT testearScrow " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi + Constantes.SERVICIO_SCROW_TESTEAR,
					request, HashMap.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT testearScrow " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR testearScrow", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> AceptarScrow(String trace) {
		LOGGER.info(trace + " INICIO AceptarScrow");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "AceptarScrow");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_DIVERTOR);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject input = new JSONObject();
			input.put("trace", trace);
			LOGGER.info(trace + " INPUT AceptarScrow " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi + Constantes.SERVICIO_SCROW_ACEPTAR,
					request, HashMap.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT AceptarScrow " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR AceptarScrow", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> DevolverScrow(String trace) {
		LOGGER.info(trace + " INICIO DevolverScrow");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "DevolverScrow");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_DIVERTOR);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject input = new JSONObject();
			input.put("trace", trace);
			LOGGER.info(trace + " INPUT DevolverScrow " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi + Constantes.SERVICIO_SCROW_CANCELAR,
					request, HashMap.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT DevolverScrow " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR DevolverScrow", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

}

package pe.com.hiper.api.serviceImpl;

import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import pe.com.hiper.api.service.HopperService;
import pe.com.hiper.common.Constantes;
import pe.com.hiper.common.UtilWeb;
import pe.com.hiper.entity.TpComponenteLog;
import pe.com.hiper.service.ComponenteLogService;

@Component
public class HopperServiceImpl implements HopperService {

private static final Logger LOGGER = LogManager.getLogger(HopperServiceImpl.class);
	
	@Value("${url.api.hopper}")
	private String urlApi;

	@Value("${kiosco.terminal}")
	private String nroTerminal;

	@Autowired
	private ComponenteLogService componenteService;
	
	@Override
	public HashMap<String, String> estadoHopper(String trace, String direccionHopper) {
		LOGGER.info(trace + " INICIO estadoHopper");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "estadoHopper");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_HOPPER);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject input = new JSONObject();
			input.put("trace", trace);
			input.put("direccionHopper", direccionHopper);
			LOGGER.info(trace + " INPUT estadoHopper " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi + Constantes.SERVICIO_HOPPER_ESTADO,
					request, HashMap.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT estadoHopper " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR estadoHopper", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> dispensarHopper(String trace, String direccionHopper, String cantidadMonedas) {
		LOGGER.info(trace + " INICIO dispensarHopper");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "dispensarHopper");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_HOPPER);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject input = new JSONObject();
			input.put("trace", trace);
			input.put("direccionHopper", direccionHopper);
			input.put("cantidadMonedas", cantidadMonedas);
			
			LOGGER.info(trace + " INPUT dispensarHopper " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi + Constantes.SERVICIO_HOPPER_DISPENSAR,
					request, HashMap.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT dispensarHopper " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR dispensarHopper", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

}

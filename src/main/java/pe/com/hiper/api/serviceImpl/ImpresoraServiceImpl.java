package pe.com.hiper.api.serviceImpl;

import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import pe.com.hiper.api.service.ImpresoraService;
import pe.com.hiper.common.Constantes;
import pe.com.hiper.common.UtilWeb;
import pe.com.hiper.entity.TpComponenteLog;
import pe.com.hiper.service.ComponenteLogService;

@Service
public class ImpresoraServiceImpl implements ImpresoraService {
	
	private static final Logger LOGGER = LogManager.getLogger(ImpresoraServiceImpl.class);
	
	@Value("${url.api.impresora}")
	private String urlApi;

	@Value("${kiosco.terminal}")
	private String nroTerminal;

	@Autowired
	private ComponenteLogService componenteService;

	@Override
	public HashMap<String, String> estadoImpresora(String trace) {
		LOGGER.info(trace + " INICIO estadoImpresora");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "estadoImpresora");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_IMPRESORA);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject input = new JSONObject();
			input.put("trace", trace);
			LOGGER.info(trace + " INPUT estadoImpresora " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi + Constantes.SERVICIO_IMPRESORA_ESTADO,
					request, HashMap.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT estadoImpresora " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR estadoImpresora", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> realizarImpresion(String datoImprimir, String dataImagen, String trace) {
		LOGGER.info(trace + " INICIO realizarImpresion");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "realizarImpresion");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_IMPRESORA);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject input = new JSONObject();
			input.put("texto", datoImprimir);
			input.put("trace", trace);
			//input.put("dataImagen", dataImagen);
			
			LOGGER.info(trace + " INPUT realizarImpresion " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi + Constantes.SERVICIO_IMPRESORA_IMPRIMIR,
					request, HashMap.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT realizarImpresion " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR realizarImpresion", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

}

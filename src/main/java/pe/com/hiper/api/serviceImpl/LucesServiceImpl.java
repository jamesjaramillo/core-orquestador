package pe.com.hiper.api.serviceImpl;

import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import pe.com.hiper.api.service.LucesService;
import pe.com.hiper.common.Constantes;
import pe.com.hiper.common.UtilWeb;
import pe.com.hiper.entity.TpComponenteLog;
import pe.com.hiper.service.ComponenteLogService;

@Component
public class LucesServiceImpl implements LucesService {
private static final Logger LOGGER = LogManager.getLogger(LucesServiceImpl.class);
	
	@Value("${url.api.luces}")
	private String urlApi;

	@Value("${kiosco.terminal}")
	private String nroTerminal;

	@Autowired
	private ComponenteLogService componenteService;
	@Override
	public HashMap<String, String> apagarLuces(String trace, String dispositivo) {
		LOGGER.info(trace + " INICIO apagarLuces");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "apagarLuces");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_LUCES);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject input = new JSONObject();
			input.put("trace", trace);
			input.put("dispositivo", dispositivo);
			LOGGER.info(trace + " INPUT apagarLuces " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi + Constantes.SERVICIO_LUCES_APAGAR,
					request, HashMap.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT apagarLuces " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR apagarLuces", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> encenderLuces(String trace, String dispositivo) {
		LOGGER.info(trace + " INICIO encenderLuces");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "encenderLuces");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_LUCES);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject input = new JSONObject();
			input.put("trace", trace);
			input.put("dispositivo", dispositivo);
			LOGGER.info(trace + " INPUT encenderLuces " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi + Constantes.SERVICIO_LUCES_ENCENDER,
					request, HashMap.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT encenderLuces " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR encenderLuces", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

}

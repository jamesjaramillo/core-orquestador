package pe.com.hiper.api.serviceImpl;

import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import pe.com.hiper.api.service.MonederoService;
import pe.com.hiper.common.Constantes;
import pe.com.hiper.common.UtilWeb;
import pe.com.hiper.entity.TpComponenteLog;
import pe.com.hiper.service.ComponenteLogService;

@Component
public class MonderoServiceImpl implements MonederoService {
	private static final Logger LOGGER = LogManager.getLogger(MonderoServiceImpl.class);
	
	@Value("${url.api.monedero}")
	private String urlApi;

	@Value("${kiosco.terminal}")
	private String nroTerminal;

	@Autowired
	private ComponenteLogService componenteService;

	@Override
	public HashMap<String, String> estadoMonedero(String trace) {
		LOGGER.info(trace + " INICIO estadoMonedero");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "estadoMonedero");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_MONEDERO);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject input = new JSONObject();
			input.put("trace", trace);
			LOGGER.info(trace + " INPUT estadoMonedero " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi + Constantes.SERVICIO_MONEDERO_ESTADO,
					request, HashMap.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT estadoMonedero " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR estadoMonedero", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> habilitarMonedero(String trace) {
		LOGGER.info(trace + " INICIO habilitarMonedero");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "habilitarMonedero");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_MONEDERO);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject input = new JSONObject();
			input.put("trace", trace);
			LOGGER.info(trace + " INPUT habilitarMonedero " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi + Constantes.SERVICIO_MONEDERO_INICIAR,
					request, HashMap.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT habilitarMonedero " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR habilitarMonedero", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> deshabilitarMonedero(String trace) {
		LOGGER.info(trace + " INICIO deshabilitarMonedero");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "deshabilitarMonedero");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_MONEDERO);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject input = new JSONObject();
			input.put("trace", trace);
			LOGGER.info(trace + " INPUT deshabilitarMonedero " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi + Constantes.SERVICIO_MONEDERO_DETENER,
					request, HashMap.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT deshabilitarMonedero " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR deshabilitarMonedero", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> obtenerDenominacion(String trace) {
		LOGGER.info(trace + " INICIO obtenerDenominacion");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "obtenerDenominacion");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_MONEDERO);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject input = new JSONObject();
			input.put("trace", trace);
			LOGGER.info(trace + " INPUT obtenerDenominacion " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi + Constantes.SERVICIO_MONEDERO_DENOMINACIO,
					request, HashMap.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT obtenerDenominacion " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR obtenerDenominacion", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

}

package pe.com.hiper.api.serviceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import pe.com.hiper.api.service.BilleteroService;
import pe.com.hiper.common.Constantes;
import pe.com.hiper.common.UtilWeb;
import pe.com.hiper.entity.TpComponenteLog;
import pe.com.hiper.model.Componente;
import pe.com.hiper.model.Moneda;
import pe.com.hiper.service.ComponenteLogService;

@Component
public class BilleteroServiceImpl implements BilleteroService {
	private static final Logger LOGGER = LogManager.getLogger(BilleteroServiceImpl.class);

	@Autowired(required = false)
	RestTemplate restTemplate;

	@Value("${url.api.billetero}")
	private String urlApi;

	@Value("${kiosco.terminal}")
	private String nroTerminal;

	@Value("${kiosco.siglaDenominacionPais}")
	private String siglaDenominacion;

	@Value("${kiosco.tipoAceptador}")
	private String tipoAceptador;

	@Autowired
	private ComponenteLogService componenteService;

	private static Map<String, Integer> mapModoComponente = new HashMap<String, Integer>();
	private static Map<String, Integer> mapCapacidadComponente = new HashMap<String, Integer>();

	private String flagRetiraBill;

	public BilleteroServiceImpl() {
		// None = 0,CashIn = 1,Dispense = 2,Recycle = 3
		mapModoComponente.put("None", 0);
		mapModoComponente.put("CashIn", 1);
		mapModoComponente.put("Dispense", 2);
		mapModoComponente.put("Recycle", 3);
		// Full = 1,High = 2,Low = 3,Empty = 4
		mapCapacidadComponente.put("OK", 0);
		mapCapacidadComponente.put("Full", 1);
		mapCapacidadComponente.put("High", 2);
		mapCapacidadComponente.put("Low", 3);
		mapCapacidadComponente.put("Empty", 4);

		flagRetiraBill = "";
	}

	@Override
	public Moneda aceptarBilletes(String trace) {
		// TODO Auto-generated method stub
		Moneda moneda = new Moneda();

		LOGGER.info(trace + " INICIO aceptarBilletes");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "aceptarBilletes");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_BILLETERO);
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject input = new JSONObject();
			input.put("trace", trace);
			LOGGER.info(trace + " INPUT aceptarBilletes " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<Moneda> result = restTemplate.postForEntity(urlApi + Constantes.SERVICIO_BILLETERO_ACEPTAR,
					request, Moneda.class);
			moneda = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT aceptarBilletes " + result.toString());
		} catch (Exception e) {
			moneda.setEstado(-1);
			moneda.setDescripcion("Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR aceptarBilletes", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}

		return moneda;
	}

	@Override
	public HashMap<String, String> cancelarScrow(String trace) {

		LOGGER.info(trace + " INICIO cancelarScrow");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "cancelarScrow");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_BILLETERO);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject input = new JSONObject();
			input.put("trace", trace);
			LOGGER.info(trace + " INPUT cancelarScrow " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi + Constantes.SERVICIO_BILLETERO_CANCELAR,
					request, HashMap.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT cancelarScrow " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR cancelarScrow", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> finalizarScrow(String trace, boolean isCancel) {
		LOGGER.info(trace + " INICIO finalizarScrow");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "finalizarScrow");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_BILLETERO);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject input = new JSONObject();
			input.put("trace", trace);
			input.put("iscancel", isCancel);
			LOGGER.info(trace + " INPUT finalizarScrow " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate
					.postForEntity(urlApi + Constantes.SERVICIO_BILLETERO_FINALIZAR, request, HashMap.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT finalizarScrow " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR finalizarScrow", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public Moneda dispensarBilletes(List<Moneda> billetes, String trace) {
		Moneda moneda = new Moneda();

		LOGGER.info(trace + " INICIO dispensarBilletes");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "dispensarBilletes");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_BILLETERO);
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject input = new JSONObject();
			input.put("trace", trace);
			input.put("billetes", billetes);
			LOGGER.info(trace + " INPUT dispensarBilletes " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<Moneda> result = restTemplate.postForEntity(urlApi + Constantes.SERVICIO_BILLETERO_DISPENSAR,
					request, Moneda.class);
			moneda = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT dispensarBilletes " + result.toString());
		} catch (Exception e) {
			moneda.setEstado(-1);
			moneda.setDescripcion("Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR dispensarBilletes", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}

		return moneda;
	}

	@Override
	public HashMap<String, String> vaciarPayout(String trace) {
		LOGGER.info(trace + " INICIO vaciarPayout");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "vaciarPayout");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_BILLETERO);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject input = new JSONObject();
			input.put("trace", trace);
			LOGGER.info(trace + " INPUT vaciarPayout " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi + Constantes.SERVICIO_BILLETERO_VACIAR,
					request, HashMap.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT vaciarPayout " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR vaciarPayout", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> resetEquipo(String trace, String tipoReinicio) {
		LOGGER.info(trace + " INICIO resetEquipo");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "resetEquipo");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_BILLETERO);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject input = new JSONObject();
			input.put("trace", trace);
			input.put("tipo", tipoReinicio);
			LOGGER.info(trace + " INPUT resetEquipo " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi + Constantes.SERVICIO_BILLETERO_RESETEAR,
					request, HashMap.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT resetEquipo " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR resetEquipo", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> solucionCashInActive(String trace) {
		LOGGER.info(trace + " INICIO solucionCashInActive");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "solucionCashInActive");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_BILLETERO);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject input = new JSONObject();
			input.put("trace", trace);
			LOGGER.info(trace + " INPUT solucionCashInActive " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<Integer> result = restTemplate
					.postForEntity(urlApi + Constantes.SERVICIO_BILLETERO_SOLCASHIN, request, Integer.class);
			int resul = result.getBody();
			data.put("estado", resul + "");
			data.put("descripcion", "ok");
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT solucionCashInActive " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR solucionCashInActive", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> cancelarUltimoComando(String trace) {
		LOGGER.info(trace + " INICIO cancelarUltimoComando");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "cancelarUltimoComando");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_BILLETERO);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject input = new JSONObject();
			input.put("trace", trace);
			LOGGER.info(trace + " INPUT cancelarUltimoComando " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<Integer> result = restTemplate
					.postForEntity(urlApi + Constantes.SERVICIO_BILLETERO_CANCEL_COMMAND, request, Integer.class);
			int resul = result.getBody();
			data.put("estado", resul + "");
			data.put("descripcion", "ok");
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT cancelarUltimoComando " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR cancelarUltimoComando", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	private HttpHeaders getHeader() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		return headers;
	}

	@Override
	public HashMap<String, String> estadoBilletero(String trace) {
		LOGGER.info(trace + " INICIO estadoBilletero");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "estadoBilletero");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_BILLETERO);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject input = new JSONObject();
			input.put("trace", trace);
			LOGGER.info(trace + " INPUT estadoBilletero " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> response = restTemplate.postForEntity(urlApi + Constantes.SERVICIO_BILLETERO_ESTADO,
					request, HashMap.class);
			LOGGER.info(trace + " OUTPUT estadoBilletero " + response.toString());

			// HashMap<String, String> rpta=response.getBody();
			data = response.getBody();

			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			if (data.get("estado").equals("2") && tipoAceptador.equals("3")) {
				configurarComponente(trace);
			}

		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR estadoBilletero", e);
		} finally {
			flagRetiraBill = "0";
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> habilitarBilletero(String trace) {
		LOGGER.info(trace + " INICIO habilitarBilletero");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "habilitarBilletero");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_BILLETERO);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject input = new JSONObject();
			input.put("trace", trace);
			LOGGER.info(trace + " INPUT habilitarBilletero " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate
					.postForEntity(urlApi + Constantes.SERVICIO_BILLETERO_HABILITAR, request, HashMap.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT habilitarBilletero " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR habilitarBilletero", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, Object> consultarComponent(String trace) {

		LOGGER.info(trace + " INICIO consultarComponent");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "consultarComponent");
		Componente componente = null;
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_BILLETERO);
		HashMap<String, Object> data = new HashMap<String, Object>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject input = new JSONObject();
			input.put("trace", trace);
			input.put("nroComponente", 5);
			LOGGER.info(trace + " INPUT consultarComponent " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<Componente> responseComponente = restTemplate
					.postForEntity(urlApi + Constantes.SERVICIO_BILLETERO_CONSULTAR_COMP, request, Componente.class);
			LOGGER.info(trace + " OUTPUT consultarComponent " + responseComponente.toString());
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());

			componente = responseComponente.getBody();
			data.put("estado", 1);
			data.put("descripcion", "ok");
			data.put("componente", componente);

		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR consultarComponent", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public List<Componente> consultarComponentes(String trace) {
		LOGGER.info(trace + " INICIO consultarComponentes");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "consultarComponentes");
		List<Componente> listaComponentes = new ArrayList<Componente>();
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_BILLETERO);
		HashMap<String, Object> data = new HashMap<String, Object>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject input = new JSONObject();
			input.put("trace", trace);
			input.put("nroComponente", 5);
			LOGGER.info(trace + " INPUT consultarComponent " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<List> responseComponente = restTemplate
					.postForEntity(urlApi + Constantes.SERVICIO_BILLETERO_CONSULTAR_COMPS, request, List.class);
			LOGGER.info(trace + " OUTPUT consultarComponentes " + responseComponente.toString());
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());

			listaComponentes = responseComponente.getBody();

		} catch (Exception e) {
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR consultarComponentes", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return listaComponentes;
	}

	@Override
	public HashMap<String, String> actualizarComponent(String trace) {
		LOGGER.info(trace + " INICIO actualizarComponent");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "actualizarComponent");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_BILLETERO);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject input = new JSONObject();
			LOGGER.info(trace + " INPUT actualizarComponent " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate
					.postForEntity(urlApi + Constantes.SERVICIO_BILLETERO_ACTUALIZAR_COMPS, request, HashMap.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT actualizarComponent " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR actualizarComponent", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> billetesPorComponente(String trace) {
		LOGGER.info(trace + " INICIO billetesPorComponente");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "billetesPorComponente");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_BILLETERO);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject input = new JSONObject();
			input.put("trace", trace);
			LOGGER.info(trace + " INPUT billetesPorComponente " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate
					.postForEntity(urlApi + Constantes.SERVICIO_BILLETERO_BILLETES_COMP, request, HashMap.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT billetesPorComponente " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR billetesPorComponente", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> configurarComponente(String trace) {
		LOGGER.info(trace + " INICIO configurarComponente");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "configurarComponente");
		String result = "";
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_BILLETERO);
		HashMap<String, String> data = new HashMap<String, String>();
		Componente componente = (Componente) consultarComponent(trace).get("componente");
		try {
			JSONObject jsonRequestConfigComp = new JSONObject();
			if (componente.getStatus().equals("Manipulated")) {

				if (flagRetiraBill.equals("1")) {
					LOGGER.info(trace + " " + "Se ha indicado que se va a retirar los billetes del cashbox");
					jsonRequestConfigComp.put("trace", trace);
					jsonRequestConfigComp.put("idComponente", 5);
					jsonRequestConfigComp.put("modoComponente", mapModoComponente.get(componente.getModo()));
					jsonRequestConfigComp.put("descripcionDenominacion", siglaDenominacion);
					jsonRequestConfigComp.put("denominacion", 0.0);
					jsonRequestConfigComp.put("contadorBill", 0);
					jsonRequestConfigComp.put("capacidad", mapCapacidadComponente.get(componente.getCapacidad()));
					jsonRequestConfigComp.put("cantidadDispensada", 0);
					jsonRequestConfigComp.put("cantidadIngresada", 0);
				} else {
					LOGGER.info(trace + " " + "Se ha indicado que NO se ha retirado los billetes del cashbox");
					jsonRequestConfigComp.put("trace", trace);
					jsonRequestConfigComp.put("idComponente", 5);
					jsonRequestConfigComp.put("modoComponente", mapModoComponente.get(componente.getModo()));
					jsonRequestConfigComp.put("descripcionDenominacion", siglaDenominacion);
					jsonRequestConfigComp.put("denominacion", 0.0);
					jsonRequestConfigComp.put("contadorBill", componente.getConteo());
					jsonRequestConfigComp.put("capacidad", mapCapacidadComponente.get(componente.getCapacidad()));
					jsonRequestConfigComp.put("cantidadDispensada", componente.getDispensados());
					jsonRequestConfigComp.put("cantidadIngresada", componente.getIngresos());
				}

			}
			RestTemplate restTemplate = new RestTemplate();

			/*
			 * JSONObject input = new JSONObject(); input.put("trace", trace);
			 */
			LOGGER.info(trace + " INPUT configurarComponente " + jsonRequestConfigComp.toString());
			HttpEntity<String> request = new HttpEntity<String>(jsonRequestConfigComp.toString(), UtilWeb.getHeader());
			ResponseEntity<String> responseComponente = restTemplate
					.postForEntity(urlApi + Constantes.SERVICIO_BILLETERO_CONFIGURAR_COMP, request, String.class);
			LOGGER.info(trace + " OUTPUT configurarComponente " + responseComponente.toString());
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());

			result = responseComponente.getBody();
			if (result.equals("0")) {
				LOGGER.info(trace + " " + "Se configuró el componente exitosamente");
				data.put("estado", "1");
				data.put("descripcion", "ok");
//				valReturn = "0";
			} else {
				LOGGER.info(trace + " " + "Ocurrió un error al configurar el componente");
				data.put("estado", "0");
				data.put("descripcion", "Error al configurar");
//				valReturn = "2";
			}

		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR configurarComponente", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public Moneda billetesIngresado(String trace) {
		LOGGER.info(trace + " INICIO billetesIngresado");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "billetesIngresado");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_BILLETERO);
		//HashMap<String, String> data = new HashMap<String, String>();
		Moneda data= new Moneda();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject input = new JSONObject();
			input.put("trace", trace);
			LOGGER.info(trace + " INPUT billetesIngresado " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<Moneda> result = restTemplate
					.postForEntity(urlApi + Constantes.SERVICIO_BILLETERO_BILLETES_INGRE, request, Moneda.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT billetesIngresado " + result.toString());
		} catch (Exception e) {
			data.setEstado(-1);
			data.setDescripcion("Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR billetesIngresado", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> conectarEquipo(String trace) {
		LOGGER.info(trace + " INICIO conectarEquipo");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "conectarEquipo");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_BILLETERO);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject input = new JSONObject();
			input.put("trace", trace);
			LOGGER.info(trace + " INPUT conectarEquipo " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate
					.postForEntity(urlApi + Constantes.SERVICIO_BILLETERO_CONECTAR, request, HashMap.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT conectarEquipo " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR conectarEquipo", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

}

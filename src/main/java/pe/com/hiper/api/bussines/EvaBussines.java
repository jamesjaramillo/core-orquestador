package pe.com.hiper.api.bussines;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.hiper.hcenter.crypto.DES.DES;

import pe.com.hiper.api.service.BilleteroService;
import pe.com.hiper.api.service.CastleService;
import pe.com.hiper.api.service.HopperService;
import pe.com.hiper.api.service.ImpresoraService;
import pe.com.hiper.api.service.MonederoService;
import pe.com.hiper.api.service.ScrowService;
import pe.com.hiper.api.service.SensorService;
import pe.com.hiper.api.service.SmartHopperService;
import pe.com.hiper.common.UtilWeb;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import org.springframework.http.HttpEntity;
import org.json.JSONObject;

@Component
public class EvaBussines {

	private final static String VALID_BILLETERO = "billetero";
	private final static String VALID_IMPRESORA = "impresora";
	private final static String VALID_MONEDERO = "monedero";
	private final static String VALID_SMARTHOPPER = "smarthopper";
	private final static String VALID_HOPPER = "dispensador";
	private final static String VALID_CASTLE = "castle";
	private final static String VALID_SENSOR = "sensor";
	private final static String VALID_SCROW = "scrow";
	private final static String VALID_CANTBILLETE = "cantbill";
	private final static String VALID_CANTMONEDA = "cantmon";
	private final static String VALID_CASHBOX = "cashbox";
	private final static String VALID_RED = "red";
	private final static String VALID_PORTALES = "portales";
	private final static String VALID_CAPACIDAD_SMH = "cantidadSMH";

	@Value("${kiosko.cantidadMaximaSmartHopper}")
	private String cantidadMaximaSmartHopper;

	@Value("${kiosco.terminal}")
	private String nroTerminal;

	@Value("${url.api.EVAadmin}")
	private String urlEVAadmin;

	@Autowired
	public SmartHopperService smhSevice;

	@Autowired
	public BilleteroService billeteroService;

	@Autowired
	public ImpresoraService impresoraService;

	@Autowired
	public MonederoService monederoService;

	@Autowired
	public HopperService hopperService;

	@Autowired
	public CastleService castleService;

	@Autowired
	public SensorService sensorService;

	@Autowired
	public ScrowService scrowService;

	@Autowired
	private Environment env;

	public List<HashMap<String, String>> validarDispositivos(String accion, String trace) {
		List<HashMap<String, String>> responseDispositivo = new ArrayList<HashMap<String, String>>();
		String[] dispositivos = accion.split(",");

		for (String dispositivo : dispositivos) {
			String parametro = "";
			String bDispositivo = dispositivo.replace(" ", "");
			System.out.println("***************** bDispositivo: " + bDispositivo);
			if (dispositivo.contains("sensor")) {
				parametro = dispositivo.substring(dispositivo.length() - 2, dispositivo.length());
				bDispositivo = "sensor";
			} else if (dispositivo.contains("cantbill")) {
				parametro = dispositivo.substring(8, dispositivo.length());
				bDispositivo = "cantbill";
			} else if (dispositivo.contains("cantmon")) {
				parametro = dispositivo.substring(7, dispositivo.length());
				bDispositivo = "cantmon";
			} else if (dispositivo.contains("red")) {
				parametro = dispositivo.substring(3, dispositivo.length());
				bDispositivo = "res";
			} else if (dispositivo.contains("portales")) {
				parametro = dispositivo.substring(8, dispositivo.length());
				bDispositivo = "portales";
			}
			switch (bDispositivo) {
			case VALID_BILLETERO:
				responseDispositivo.add(validarBilletero(trace));
				break;

			case VALID_IMPRESORA:
				responseDispositivo.add(validarImpresora(trace));
				break;

			case VALID_MONEDERO:
				responseDispositivo.add(validarMonedero(trace));
				break;

			case VALID_SMARTHOPPER:
				responseDispositivo.add(validarSmartHopper(trace));
				break;

			case VALID_HOPPER:
				responseDispositivo.add(validarHopper(trace));
				break;

			case VALID_CASTLE:
				responseDispositivo.add(validarCastle(trace));
				break;

			case VALID_SENSOR:
				responseDispositivo.add(validarEstadoSensor(trace, parametro));
				break;

			case VALID_SCROW:
				responseDispositivo.add(validarScrow(trace));
				break;

			case VALID_CAPACIDAD_SMH:
				responseDispositivo.add(validarEspacioSMH(trace));
				break;

			case VALID_CANTBILLETE:

				break;

			case VALID_CANTMONEDA:

				break;

			case VALID_CASHBOX:

				break;

			case VALID_RED:

				break;

			case VALID_PORTALES:

				break;
			default:
				break;
			}
		}

		return responseDispositivo;
	}

	public HashMap<String, String> validarSmartHopper(String trace) {
		HashMap<String, String> response = new HashMap<String, String>();
		HashMap<String, String> data = smhSevice.estadoSMH(trace);
		response.put("dispositivo", "SMARTHOPPER");
		if (data.containsKey("estado") && data.containsKey("descripcion")) {
			response.put("estado", data.get("estado"));

			if (response.get("estado").toString().equals("0")) {
				response.put("descripcion", "Activo");
			} else if (response.get("estado").toString().equals("1")) {
				response.put("descripcion", "Inactivo");
			} else {
				response.put("descripcion", data.get("descripcion"));
			}
		} else {
			response.put("estado", "-1");
			response.put("descripcion", "Error respuesta SmartHopper");
		}

		return response;
	}

	private HashMap<String, String> validarImpresora(String trace) {
		HashMap<String, String> response = new HashMap<String, String>();
		HashMap<String, String> data = impresoraService.estadoImpresora(trace);
		response.put("dispositivo", "IMPRESORA");

		if (data.containsKey("estado")) {
			response.put("estado", data.get("estado").toString());
			if (data.get("estado").toString().equals("0")) {
				response.put("descripcion", "Activo");
			} else {
				try {

					Thread.sleep(500);
					data = impresoraService.estadoImpresora(trace);
					response.put("estado", data.get("estado").toString());
					response.put("descripcion", data.get("descripcion").toString());
					if (data.get("estado").toString().equals("0")) {
						response.put("descripcion", "Activo");
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		} else {
			response.put("estado", "-1");
			response.put("descripcion", "Error conexion");
		}
		return response;

	}

	public HashMap<String, String> obtenerEstadoKiosco(String trace) {
		HashMap<String, String> data = new HashMap<String, String>();
		data.put("estado", UtilWeb.KIOSCO_EN_USO ? "1" : "0");
		data.put("descripcion", UtilWeb.KIOSCO_EN_USO ? "kiosco en uso" : "Kiosco disponible");
		return data;
	}

	public HashMap<String, String> setearEstadoKiosco(String trace, String estado) {
		HashMap<String, String> data = new HashMap<String, String>();
		UtilWeb.KIOSCO_EN_USO = estado.equals("1") ? false : true;
		data.put("estado", estado.equals("1") ? "1" : "0");
		data.put("descripcion", UtilWeb.KIOSCO_EN_USO ? "kiosco en uso" : "Kiosco disponible");
		return data;
	}

	public HashMap<String, Object> validarTicketAdmin(String trace, String usuario, boolean registrarUsuario) {
		HashMap<String, Object> output = new HashMap<String, Object>();
		output.put("estado", "0");
		output.put("encontrado", false);
		BufferedReader b = null;

		try {
			String directorio_properties = System.getProperty("CORE_HOME");
			if (directorio_properties == null) {
				directorio_properties = "c:" + File.separator + "properties";
			}

			DES desEncryptor = new DES();
			FileReader file = new FileReader(directorio_properties.concat(File.separator).concat("usuarios.txt"));
			b = new BufferedReader(file);

			String cadena = "";
			while ((cadena = b.readLine()) != null && !(boolean) output.get("encontrado")) {
				String usuarioEnc = desEncryptor.cipher(usuario.toUpperCase(), UtilWeb.KEY1);
				usuarioEnc = desEncryptor.decipher(usuarioEnc.toUpperCase(), UtilWeb.KEY2);
				usuarioEnc = desEncryptor.cipher(usuarioEnc.toUpperCase(), UtilWeb.KEY3);

				if (usuarioEnc.equals(cadena.substring(0, cadena.length() - 1))) {
					System.out.println("encontrado");
					output.put("encontrado", true);

					String acceso = env
							.getProperty("usuario.accesoRol" + cadena.substring(cadena.length() - 1, cadena.length()));
					System.out.println("LA RESPUESTA DEL ROL");
					String[] temp = acceso.split(";");
					output.put("nomUsuario", temp[0]);
					output.put("acceso", temp[1]);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return output;
	}

	private HashMap<String, String> validarMonedero(String trace) {
		HashMap<String, String> data = new HashMap<String, String>();

		HashMap<String, String> resultado = monederoService.estadoMonedero(trace);
		System.out.println("*****VALIDADOR DE MONEDERO*****");
		data.put("dispositivo", "MONEDERO");
		System.out.println(resultado);

		if (resultado.get("estado").equals("0")) {
			data.put("estado", "0");
			data.put("descripcion", "Activo");
		} else if (resultado.get("estado").equals("1")) {
			data.put("estado", "1");
			data.put("descripcion", "Sin conexión");
		} else if (resultado.get("estado").equals("2")) {
			data.put("estado", "2");
			data.put("descripcion", "Error");
		} else {
			data.put("estado", resultado.get("estado"));
			data.put("descripcion", "No se estableció la conexión con el servicio de dispositivos");
		}

		return data;
	}

	private HashMap<String, String> validarHopper(String trace) {
		HashMap<String, String> data = new HashMap<String, String>();
		data.put("dispositivo", "HOPPER");
		HashMap<String, String> resultado = hopperService.estadoHopper(trace,
				env.getProperty("dispositivo.hopper.canal.hopper1"));
		switch (resultado.get("estado")) {
		case "0":
			data.put("estado", "0");
			data.put("descripcion", "Funcional");
			break;

		case "1":
			data.put("estado", "1");
			data.put("descripcion", "No Habilitable");
			break;

		case "2":
			data.put("estado", "2");
			data.put("descripcion", "No responde adecuadamente");
			break;

		default:
			data.put("estado", "-2");
			data.put("descripcion", "No se estableció la conexión con el servicio de dispositivos");
			break;
		}

		return data;
	}

	private HashMap<String, String> validarCastle(String trace) {
		HashMap<String, String> data = new HashMap<String, String>();

		HashMap<String, String> respuesta = castleService.estadoCastle(trace);
		respuesta.put("dispositivo", "CASTLE");
		if (respuesta != null && respuesta.get("estado").equals("0")) {
			HashMap<String, String> estadoCastle = castleService.obtenerDatosCastle(trace);
			data.put("estado", estadoCastle.get("estado"));
			data.put("descripcion", estadoCastle.get("descripcion"));
			data.put("dispositivo", "CASTLE");
			data.put("numSerie", estadoCastle.get("numSerie"));
		} else {

			data.put("estado", "-1");
			data.put("descripcion", "Dispositivo no encontrado");
			data.put("dispositivo", "CASTLE");
			data.put("numSerie", "xxxxxxxxx");
		}

		return data;
	}

	private HashMap<String, String> validarBilletero(String trace) {
		HashMap<String, String> datos = new HashMap<String, String>();
		// mensajeDispositivo.setDispositivo("RECICLADOR DE BILLETES");

		HashMap<String, String> estadoFujitsu = billeteroService.estadoBilletero(trace);

		datos.put("estado", estadoFujitsu.get("estado"));
		datos.put("dispositivo", "BILLETERO");

		// 32000 - 7D00 - CANCEL LAST COMMAND
		// 4609 - 1201 - TIMEOUT CASH IN
		if (datos.get("estado").equals("0") || datos.get("estado").equals("32000")
				|| datos.get("estado").equals("65509")) {
			datos.put("estado", "0");
			datos.put("descripcion", "Activo");
		} else if (datos.get("estado").equals("1")) {
			datos.put("descripcion", "Atascado");
		} else if (datos.get("estado").equals("2")) {

			datos.put("descripcion", "Error");
		} else if (datos.get("estado").equals("3")) {
			datos.put("descripcion", "Apagado");
		} else {
			datos.put("descripcion", "Error");
		}

		return datos;
	}

	private HashMap<String, String> validarEstadoSensor(String trace, String numSensor) {

		// MensajeDispositivo mensajeDispositivo = new MensajeDispositivo();
		HashMap<String, String> datos = new HashMap<String, String>();

		// ResourceBundle rb = Funciones.getResourceBundleCDispositivos();

		if (numSensor.equals(env.getProperty("dispositivo.sensor.puerta"))) {
			datos.put("dispositivo", "PUERTA");
		} else if (numSensor.equals(env.getProperty("dispositivo.sensor.smartHopper"))) {
			datos.put("dispositivo", "RECICLADOR MONEDAS-S");
		} else if (numSensor.equals(env.getProperty("dispositivo.sensor.custodianBag1"))) {
			datos.put("dispositivo", "CUSTODIAN BAG 1");
		} else if (numSensor.equals(env.getProperty("dispositivo.sensor.custodianBag2"))) {
			datos.put("dispositivo", "CUSTODIAN BAG 2");
		} else if (numSensor.equals(env.getProperty("dispositivo.sensor.puerta1"))) {
			datos.put("dispositivo", "PUERTA 1");
		} else if (numSensor.equals(env.getProperty("dispositivo.sensor.sensorHopper"))) {
			datos.put("dispositivo", "SENSOR HOPPER");
		} else {
			datos.put("dispositivo", "SENSOR" + numSensor);
		}

		try {
			// HashMap<String, String> resultado =
			// this.getDispositivoService().estadoSensores(trace, numSensor);
			HashMap<String, String> resultado = sensorService.estadoSensor(trace, numSensor);

			datos.put("estado", resultado.get("estado"));

			if (numSensor.equals(env.getProperty("dispositivo.sensor.puerta"))) {
				datos.put("descripcion", resultado.get("estado").equals("1") ? "Cerrada" : "Abierta");
			} else if (numSensor.equals(env.getProperty("dispositivo.sensor.puerta1"))) {
				datos.put("descripcion", resultado.get("estado").equals("1") ? "Cerrada" : "Abierta");
			} else {
				datos.put("descripcion", resultado.get("estado").equals("1") ? "Ok" : "Removido");
			}
		} catch (Exception e) {
			datos.put("estado", "-1");
			datos.put("descripcion", "Error general");
		}

		return datos;
	}

	private HashMap<String, String> validarEspacioSMH(String trace) {
		HashMap<String, String> data = new HashMap<String, String>();
		
		data.put("dispositivo", "RECICLADOR MONEDAS-E");
		

		JSONObject jsonRequest = new JSONObject();
		jsonRequest.put("trace_number", trace);

		try{
			int umbralSmartHopper = Integer.parseInt(cantidadMaximaSmartHopper);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<String> request = new HttpEntity<String>(jsonRequest.toString(), UtilWeb.getHeader());
			ResponseEntity<String> result = restTemplate.postForEntity(urlEVAadmin + "admin/tramaSMH",
				request, String.class);

			String body = result.getBody();
			JSONObject jsonObject = new JSONObject(body);
			String tramaSmartHopper = jsonObject.get("tramaSmartHopper").toString();
			System.out.println("*** TramaSmartHopper: " + tramaSmartHopper);

			int cantSmartHopper = 0;
			if( !tramaSmartHopper.equals("") ){
				String[] denomCashBox = tramaSmartHopper.split(":");
				for (String denom : denomCashBox) {
					String[] datosDenom = denom.split(";");
					cantSmartHopper += Integer.parseInt(datosDenom[1]);
				}
			}else{
				cantSmartHopper = umbralSmartHopper;
			}

			System.out.println("*** CantSmartHopper: " + cantSmartHopper);

			if(cantSmartHopper>=umbralSmartHopper){
				data.put("estado", "1");
				data.put("descripcion", "Lleno");
			}else{
				data.put("estado", "0");
				data.put("descripcion", "Con espacio");
			}
		}catch (Exception e) {
			System.out.println(trace + "validarEspacioSMH" + e.getMessage());
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
		}
		return data;
	}

	public HashMap<String, String> validarScrow(String trace) {
		HashMap<String, String> datos = new HashMap<String, String>();
		datos.put("dispositivo", "SCROW");
		HashMap<String, String> resultado = scrowService.testearScrow(trace);

		datos.put("estado", resultado.get("estado"));
		if (datos.get("estado").equals("0")) {
			datos.put("descripcion", "Inactivo");
		} else if (datos.get("estado").equals("1")) {
			datos.put("descripcion", "Activo");
		} else {
			datos.put("descripcion", resultado.get("descripcion"));
		}

		return datos;
	}

}

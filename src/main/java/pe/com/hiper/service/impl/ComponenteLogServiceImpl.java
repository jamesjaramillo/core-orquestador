package pe.com.hiper.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.com.hiper.entity.TpComponenteLog;
import pe.com.hiper.repository.ComponenteLogRepository;
import pe.com.hiper.service.ComponenteLogService;

@Service
public class ComponenteLogServiceImpl implements ComponenteLogService {

	@Autowired
	ComponenteLogRepository repository;
	
	@Override
	public TpComponenteLog saveComponente(TpComponenteLog componente) {
		// TODO Auto-generated method stub
		return repository.save(componente);
	}

}
